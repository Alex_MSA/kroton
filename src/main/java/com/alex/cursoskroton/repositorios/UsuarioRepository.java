package com.alex.cursoskroton.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alex.cursoskroton.entidades.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {}
