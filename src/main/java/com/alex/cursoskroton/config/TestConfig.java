package com.alex.cursoskroton.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.alex.cursoskroton.entidades.Usuario;
import com.alex.cursoskroton.repositorios.UsuarioRepository;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public void run(String... args) throws Exception {
		Usuario user1 = new Usuario(null, "Alex", "alex2016.sm@gmail.com", "99993333", "12345");
		Usuario user2 = new Usuario(null, "Joaquim", "joaquim@joaquim.com", "99994444", "1234567");
	
	usuarioRepository.saveAll(Arrays.asList(user1, user2));
	}

}
