package com.alex.cursoskroton.recursos;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alex.cursoskroton.entidades.Usuario;
import com.alex.cursoskroton.servicos.UsuarioService;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService servicos;
	
	@GetMapping
	public ResponseEntity<List<Usuario>> getTodos(){
		List<Usuario> lista = servicos.findAll();
		return ResponseEntity.ok().body(lista);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Usuario>getPorId(@PathVariable Long id){
		Usuario usuarioEncontrado = servicos.findById(id);
		return ResponseEntity.ok().body(usuarioEncontrado);
	}
}
