package com.alex.cursoskroton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoskrotonApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoskrotonApplication.class, args);
	}

}
